<div>
    <button>Add</button>
    <table >
    @foreach ($users as $user)
    @if($user->role == "shop")
    <tr>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->id}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->name}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->email}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->role}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
            <a class="nav-link" >
              <span class="menu-title">edit</span>
            </a>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
            <a class="nav-link" >
              <span class="menu-title">delete</span>
            </a>
        </td>
        </td>
    </tr>
    @endif
    @endforeach
    </table>
    <div>
    <div class="panel-body">
        <form class="form-horizontal" wire:submit.prevent="storeUser">
            @if(Session::has('message'))
            <div class="alert alert-succes" role="alert">{{Session::get('message')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
            @endif
            <div class="form-group">
                <label class="col-md-4 control-label">Name</label>
                <input type="text" placeholder="name" class="form-control input md" wire:model="name" wire:keyup="">
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Account</label>
                <input type="text" placeholder="name" class="form-control input md" wire:model="account" wire:keyup="">
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Role</label>
                <input type="text" placeholder="name" class="form-control input md" wire:model="role" wire:keyup="">
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Shop Name</label>
                <input type="text" placeholder="name" class="form-control input md"wire:model="shop_name" wire:keyup="">
            </div>
            <div class="form-group">
            <label class="col-md-4 control-label"></label>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"></button>
            </div>
            </div>
        </form>
    </div>
    </div>
    
</div>
