
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">							
            <div class=" main-content-area">
                <div class="wrap-login-item ">
                    <div class="register-form form-item ">
                    <!-- <x-jet-validation-errors class="mb-4" /> -->
                        <form class="form-stl"  name="frm-login"  wire:submit.prevent="createUser">
                            
                            <fieldset class="wrap-title">
                                <h3 class="form-title">Create an account</h3>
                                <h4 class="form-subtitle">Personal infomation</h4>
                            </fieldset>									
                            <fieldset class="wrap-input">
                                <label for="frm-reg-lname">Name*</label>
                                <input type="text"  placeholder="Last name*" required  wire:model="name" wire:keyup="">
                            </fieldset>
                            <fieldset class="wrap-input">
                                <label for="frm-reg-email">Email Address*</label>
                                <input type="email"  placeholder="Email address" required  wire:model="account" wire:keyup="">
                            </fieldset>
                            <!-- <fieldset class="wrap-functions ">
                                <label class="remember-field">
                                    <input name="newletter" id="new-letter" value="forever" type="checkbox"><span>Sign Up for Newsletter</span>
                                </label>
                            </fieldset> -->
                            <fieldset class="wrap-title">
                                <h3 class="form-title">Login Information</h3>
                            </fieldset>
                            <fieldset class="wrap-input item-width-in-half left-item ">
                                <label for="frm-reg-pass">Password *</label>
                                <input type="text"  placeholder="Password" required wire:model="password" wire:keyup="">
                            </fieldset>
                            <fieldset class="wrap-input item-width-in-half ">
                                <label for="frm-reg-cfpass">Confirm Password *</label>
                                <input type="text"  placeholder="Confirm Password" require autocomplete="new-password" wire:model="retype" wire:keyup="">
                            </fieldset>
                            <button type="submit" class="btn btn-primary" >Register</button>
                            @if(Session::has('message'))
                            <div class="alert alert-succes" role="alert">{{Session::get('message')}}</div>
                            @endif
                            @if(Session::has('error'))
                            <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
                            @endif
                            
                            <div class="row mx-gutters-2 mb-4">
                                <div class="col-sm-4">
                                    <a href="{{ route('login.google') }}">
                                        <button type="button" class="btn btn-block btn-google">
                                            <i class="fa fa-google mr-2"></i>Google
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="flex items-center justify-end mt-4">
                                <a class="ml-1 btn btn-primary" href="{{ url('auth/facebook') }}" 
                                style="margin-top: 0px !important;background: blue;color: #ffffff;padding: 5px;border-radius:7px;" id="btn-fblogin">
                                    <i class="fa fa-facebook-square" aria-hidden="true"></i> Login with Facebook
                                </a>
                            </div>
                        </form>
                    </div>											
                </div>
            </div><!--end main products area-->		
        </div>
    </div><!--end row-->






<div class="register-form form-item ">
    <button>Add</button>
    <table >
    @foreach ($users as $user)
    
    <tr>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->id}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->name}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->email}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
              <span class="menu-title">{{$user->role}}</span>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
            <a class="nav-link" >
              <span class="menu-title">edit</span>
            </a>
        </td>
        </td>
        <td class="nav-item menu-items">
            <td class="nav-link">
            <a class="nav-link" >
              <span class="menu-title">delete</span>
            </a>
        </td>
        </td>
    </tr>
    
    @endforeach
    </table>
    <div>
    <div class="panel-body">
        <form class="form-horizontal" wire:submit.prevent="storeUser">
            @if(Session::has('message'))
            <div class="alert alert-succes" role="alert">{{Session::get('message')}}</div>
            @endif
            @if(Session::has('error'))
            <div class="alert alert-danger" role="alert">{{Session::get('error')}}</div>
            @endif
            <div class="form-group">
                <label class="col-md-4 control-label">Name</label>
                <input type="text" placeholder="name" class="form-control input md" wire:model="name" wire:keyup="">
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Account</label>
                <input type="text" placeholder="name" class="form-control input md" wire:model="account" wire:keyup="">
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Role</label>
                <input type="text" placeholder="name" class="form-control input md" wire:model="role" wire:keyup="">
            </div>
            <div class="form-group">
                <label class="col-md-4 control-label">Shop Name</label>
                <input type="text" placeholder="name" class="form-control input md"wire:model="shop_name" wire:keyup="">
            </div>
            <div class="form-group">
            <label class="col-md-4 control-label"></label>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"></button>
            </div>
            </div>
        </form>
    </div>
    </div>
    
</div>
