<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'google' => [
        'client_id'=>'675567238525-cg78tuedhku77lv50c31p9hd2pu88vdd.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect' => 'http://localhost:8004/login/google/callback',
    ],
    'facebook' => [
        'client_id' => '712338263477523',
        'client_secret' => 'f67cb85ede6f39b21f9c92184bca845c',
        'redirect' => 'https://localhost:8004/auth/facebook/callback',
    ],

];
