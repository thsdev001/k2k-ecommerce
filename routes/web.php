<?php

use App\Http\Livewire\Admin\AdminDashboard;
use App\Http\Livewire\CartComponent;
use App\Http\Livewire\CheckoutComponent;
use App\Http\Livewire\HomeComponent;
use App\Http\Livewire\DetailsComponent;
use App\Http\Livewire\LoginComponent;
use App\Http\Livewire\RegisterComponent;
use App\Http\Livewire\ShopComponent;
use App\Http\Livewire\ShopManagerComponent;
use App\Http\Livewire\User\UserDashboard;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\FacebookController;
use App\Http\Livewire\DetailsComponent;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/',HomeComponent::class)->name('Home');
Route::get('/Home',HomeComponent::class);
Route::get('/Shop',ShopComponent::class);
Route::get('/Cart',CartComponent::class)->name('product.cart');
Route::get('/Checkout',CheckoutComponent::class);
Route::get('/product/{slug}',DetailsComponent::class)->name('product.details');
// Route::get('login',LoginComponent::class);
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/admin/dashboard',AdminDashboard::class)->name('admin.dashboard');
});
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/user/dashboard',UserDashboard::class)->name('user.dashboard');
});
Route::get('/register-user',RegisterComponent::class)->name('register');
Route::get('/ShopManager',ShopManagerComponent::class);
// Google login
Route::get('login/google', [App\Http\Controllers\Auth\LoginController::class, 'redirectToGoogle'])->name('login.google');
Route::get('login/google/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleGoogleCallback']);
Route::get('/product/{slug}',DetailsComponent::class)->name('product.details');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Facebook login
Route::get('auth/facebook', [FacebookController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [FacebookController::class, 'handleFacebookCallback']);
