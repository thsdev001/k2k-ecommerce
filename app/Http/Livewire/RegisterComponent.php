<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterComponent extends Component
{
    public $name;
    public $account;
    public $password;
    public $retype;
    public function createUser()
    {
        if($this->password === $this->retype){
            $user = new User();
        $user->name = $this->name;
        $user->email = $this->account;
        $user->role = 'user';
        $user->password = Hash::make($this->password);
        
        $this->_registerOrLoginUser($user);
        return redirect()->route('Home');
        }else{
            session()->flash('message','re-type password is incorrect!');
        }
        
        
    }
    protected function _registerOrLoginUser($data)
    {
        $user = User::where('email', '=', $data->email)->first();
        if (!$user) {
            $user = new User();
        $user->name = $this->name;
        $user->email = $this->account;
        $user->role = 'user';
        $user->password = Hash::make($this->password);
            $user->save();
        }

        Auth::login($user);
    }
    // $user = User::where('email', '=', $data->email)->first();
    //     if (!$user) {
    //         $user = new User();
    //         $user->name = $data->name;
    //         $user->email = $data->email;
    //         $user->provider_id = $data->id;
    //         $user->avatar = $data->avatar;
    //         $user->save();
    //     }
    public function render()
    {
        $users = User::all();
        return view('livewire.register-component',['users'=>$users])->layout('layouts.base');
    }
}
