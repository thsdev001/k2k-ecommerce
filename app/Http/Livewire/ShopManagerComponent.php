<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

use Illuminate\Support\Facades\Hash;

class ShopManagerComponent extends Component
{
    public $name;
    public $account;
    public $role;
    public $shop_name;
    
    public function storeUser()
    {
        
        $user = new User();
        $user->name = $this->name;
        $user->email = $this->account;
        $user->role = $this->role;
        $user->shop_name = $this->shop_name;
        $user->password = Hash::make('123456a@A');
        $user->save();
        session()->flash('message','User has been created successfully!');
    }
    //    $num = 0;
    //    $nameadd = $this->name;$accountadd = $this->account;
    //    $roleadd = $this->role;$shop_nameadd = $this->shop_name;
    //    $user = new User();
    //    $users = User::all();
    //     if($nameadd != '' && $accountadd != '' && $roleadd != '' && $shop_nameadd != '')
    //     {
    //     foreach($users as $usersss){
    //         if($usersss->name != $nameadd)
    //         {
    //            $num++;
    //         }
    //     }
    //     if($num = 0)
    //     {
    //         $user->name = $this->name;
    //         $user->email = $this->account;
    //         $user->role = $this->role;
    //         $user->shop_name = $this->shop_name;
    //         $user->password = Hash::make('123456a@A');
    //         $user->save();
            
    //         session()->flash('message','User has been created successfully!');
    //     }else{
    //         $user->delete();
    //         session()->flash('message','User existed');
    //     }
       
    //     }else {
    //         $user->delete();
    //         session()->flash('message','dont accept null value');
    //     }
    // }
    // public function deleteUser()
    // {
    //     $user = new User();
    //     $user->name = $this->name;
    //     $user->email = $this->account;
    //     $user->role = $this->role;
    //     $user->shop_name = $this->shop_name;
    //     $user->password = Hash::make('123456a@A');
    //     $user->save();
    //     session()->flash('message','User has been created successfully!');
    // }
    public function render()
    {
        $users = User::all();
        return view('livewire.shop-manager-component',['users'=>$users])->layout('layouts.dashboard');
    }
}


