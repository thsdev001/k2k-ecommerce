-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2022 at 08:03 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `k2kcommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'perspiciatis velit', 'perspiciatis-velit', '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(2, 'in similique', 'in-similique', '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(3, 'voluptate praesentium', 'voluptate-praesentium', '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(4, 'nostrum deleniti', 'nostrum-deleniti', '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(5, 'eum error', 'eum-error', '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(6, 'ut suscipit', 'ut-suscipit', '2022-03-13 02:17:21', '2022-03-13 02:17:21');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `categoryid` int(11) NOT NULL,
  `categoryname` text NOT NULL,
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `created_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`categoryid`, `categoryname`, `updated_at`, `created_at`) VALUES
(223, 'Tiện tích', '2022-03-10 04:06:14', '2022-03-10 04:06:14'),
(230, 'Gia dụng', '2022-03-10 04:05:35', '2022-03-10 04:05:35'),
(231, 'Công Nghệ', '2022-03-10 04:05:35', '2022-03-10 04:05:35'),
(255, 'Công sở', '2022-03-10 04:08:11', '2022-03-10 04:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `manufacturer`
--

CREATE TABLE `manufacturer` (
  `manufacturerid` int(11) NOT NULL,
  `manufacturername` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `manufacturer`
--

INSERT INTO `manufacturer` (`manufacturerid`, `manufacturername`) VALUES
(1000, 'Samsung'),
(1001, 'Apple'),
(1002, 'Xiaomi'),
(1003, 'Panasonic'),
(1004, 'Sony'),
(1005, 'LG'),
(1006, 'Sharp');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2022_02_25_154927_create_sessions_table', 1),
(36, '2019_12_14_000001_create_personal_access_tokens_table', 2),
(37, '2022_03_07_182605_create_categories_table', 2),
(38, '2022_03_07_182716_create_products_table', 2),
(41, '2022_03_12_154018_create_categories_table', 3),
(42, '2022_03_12_154033_create_products_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `productid` int(11) NOT NULL,
  `productname` text NOT NULL,
  `price` float NOT NULL,
  `date` date NOT NULL,
  `description` text NOT NULL,
  `type` int(11) NOT NULL,
  `manufacturerid` int(11) NOT NULL,
  `categoryid` int(11) DEFAULT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`productid`, `productname`, `price`, `date`, `description`, `type`, `manufacturerid`, `categoryid`, `image`) VALUES
(277, 'IPhone XYZ', 658568, '2022-03-17', 'iPhone X 256GB Quốc Tế Cũ Like New 99%', 203, 1002, 230, 'xiaomiredminote8pro.jpg'),
(2000, 'iPhone X', 8690000, '2021-03-15', 'iPhone X 256GB Quốc Tế Cũ Like New 99%\r\nCòn hàng', 200, 1001, 231, 'iphone-x-64gb-hh-600x600.jpg'),
(2001, 'iPhone Xs', 13590000, '2019-08-25', 'iPhone Xs 64GB Quốc Tế New 100%\r\n', 200, 1001, 231, 'iphone-xs-vang-600x600-600x600.jpg'),
(2002, 'iPhone Xs Max', 11590000, '2020-09-06', 'iPhone Xs Max 64GB Cũ Like New 99%\r\n', 200, 1001, 231, 'iphone-xs-vang-600x600-600x600.jpg'),
(2003, 'iPhone Xr', 11290000, '2020-08-02', 'iPhone XR 64GB mới 100% fullbox\r\n', 200, 1001, 231, 'tải xuống.jpg'),
(2004, 'iPhone 8 Plus', 6990000, '2018-08-13', 'iPhone 8 Plus 64GB Quốc Tế Cũ Like New 99%\r\nHet hang', 200, 1001, 231, '1115679429.jpeg'),
(2005, 'Samsung Galaxy S10 Plus', 8990000, '2019-12-09', 'Samsung Galaxy S10 Plus Like New 99% (8GB - 128GB)\r\nCòn hàng', 200, 1000, 231, 'samsung-galaxy-s10-plus-white-600x600.jpg'),
(2006, 'Samsung Galaxy Note 10 Plus', 13990000, '2020-03-10', 'Samsung Galaxy Note 10 Plus 256GB Mới 100%\r\nCòn hàng', 200, 1000, 231, 'samsung-galaxy-note-10-plus-silver-new-600x600.jpg'),
(2007, 'Samsung Galaxy Note 9', 8590000, '2018-04-16', 'Samsung Galaxy Note 9 Like New 99% (6GB - 128GB)\r\nCòn hàng', 200, 1000, 231, 'samsung-galaxy-note-9-black-600x600.jpg'),
(2008, 'Samsung Galaxy S9 Plus', 5190000, '2019-07-26', 'Samsung Galaxy S9 Plus Mỹ Like New cũ 99%\r\n', 200, 1000, 231, 'samsung-galaxy-s9-black-600x600.jpg'),
(2009, 'Samsung Galaxy S20 Plus', 13990000, '2019-01-25', 'Samsung Galaxy S20 Plus (8GB - 128GB)', 200, 1000, 231, 'samsung-galaxy-s20-plus-xam-600x600-200x200.jpg'),
(2010, 'Tai nghe i27 Pro Max 2020', 399000, '2020-10-20', 'i27 Pro Max 2020 - Bản sao của Airpods Pro\r\n', 207, 1002, 231, 'i27-pro-max-2020-3.jpg'),
(2011, 'Tai nghe i27 TWS', 349000, '2020-05-17', 'Tai nghe i27 TWS (Cảm biến trong tai)\r\n', 207, 1002, 231, '3_1583463826.jpg'),
(2012, 'Tai nghe i27 Pro Max ', 579000, '2021-02-16', 'Còn hàng', 207, 1002, 231, 'i27promax.jpg'),
(2013, 'Tai nghe i27 Pro TWS', 499000, '2019-12-20', 'Còn hàng', 207, 1002, 231, 'i27pro.jpg'),
(2014, 'Tai nghe i27 Pro Mini TWS', 399000, '2020-03-12', 'Còn hàng', 207, 1002, 231, 'i27promini.jpg'),
(2015, 'Tai nghe Apple Airpods 2\r\n', 3350000, '2020-06-24', 'Tặng Case bảo vệ thời trang\r\nShip hàng toàn quốc, ship COD nhận hàng mới thanh toán\r\nCòn hàng', 207, 1002, 231, 'airpod2.jpg'),
(2016, 'Tai nghe Apple Airpods Pro\r\n', 6290000, '2019-06-15', 'Tặng Case bảo vệ thời trang\r\nShip hàng toàn quốc, ship COD nhận hàng mới thanh toán\r\nCòn hàng', 207, 1002, 231, 'airpodpro.jpg'),
(2017, 'iPhone 11 Pro\r\n', 15990000, '2021-03-03', 'Còn hàng', 200, 1001, 231, 'iphone11pro.jpg'),
(2018, 'iPhone 11 Pro Max', 18290000, '2020-02-25', 'Hàng Like New', 200, 1001, 231, 'iphone11promax.jpg'),
(2019, 'iPhone 11\r\n', 14990000, '2019-02-15', 'Hàng Like New\r\nCòn hàng', 200, 1001, 231, 'iphone11.jpg'),
(2020, 'iPhone 12\r\n', 20590000, '2020-04-28', 'Hàng mới\r\nChính hãng VN/A', 200, 1001, 231, 'iphone12.jpg'),
(2021, 'iPhone 12 Pro\r\n', 26790000, '2019-06-12', 'Hàng mới\r\nChính hãng VN/A', 200, 1001, 231, 'iphone12pro.jpg'),
(2022, 'iPhone 12 Pro Max', 29790000, '2020-08-11', 'Còn hàng\r\nHàng Like New', 200, 1001, 231, 'iphone12promax.jpg'),
(2023, 'iPhone 8', 4390000, '2018-01-15', 'Hàng Like New\r\nMiễn phí vận chuyển', 200, 1001, 231, 'iphone8.jpg'),
(2024, 'iPhone 7', 3590000, '2018-07-28', 'Hàng Like New', 200, 1001, 231, 'iphone7.jpg'),
(2025, 'iPhone 7 Plus', 4990000, '2019-05-12', 'Hàng Like New', 200, 1001, 231, 'iphone7plus.jpg'),
(2026, 'iPhone 6', 2990000, '2017-08-06', 'Hàng Like New', 200, 1001, 231, 'iphone6.jpg'),
(2027, 'iPhone 6s', 3490000, '2018-11-12', 'Còn hàng\r\nHàng Like New', 200, 1001, 231, 'iphone6s.jpg'),
(2028, 'iPhone 6s Plus', 4690000, '2020-07-22', 'Hàng Like New', 200, 1001, 231, 'iphone6plus.jpg'),
(2029, 'Samsung Galaxy A90 5G', 6190000, '2021-03-12', 'Hàng Like New', 200, 1000, 231, 'samsunggalaxya905g.jpg'),
(2030, 'Samsung Galaxy S8', 4190000, '2020-06-24', 'Hàng Like New', 200, 1000, 230, 'samsunggalaxys8.jpg'),
(2031, 'Xiaomi Redmi K20', 4990000, '2021-01-12', 'Hàng Like New', 200, 1002, 230, 'xiaomiredmik20propremium.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `regular_price` decimal(8,2) NOT NULL,
  `sale_price` decimal(8,2) DEFAULT NULL,
  `SKU` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_status` enum('instock','outofstock') COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 10,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `images` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `slug`, `short_description`, `description`, `regular_price`, `sale_price`, `SKU`, `stock_status`, `featured`, `quantity`, `image`, `images`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 'consequatur occaecati aut enim', 'consequatur-occaecati-aut-enim', 'Numquam adipisci ratione exercitationem distinctio. Vel ut impedit molestiae atque. Minima qui nam deserunt enim et provident. Sed corporis similique facilis deleniti asperiores.', 'Qui aut rerum voluptatum et odit esse. Quae et sequi rerum tempore necessitatibus iusto molestias. Qui deleniti odit sint neque dolores similique. Quod laborum doloribus harum a. Quo occaecati aut magni et rerum. Quo commodi dolore rerum accusamus. Qui dolorem esse et natus ea inventore inventore. Est consequuntur vitae laborum unde. Dolor illo aut nostrum ipsam voluptatem eos. Eum quibusdam aliquid nihil exercitationem veniam sunt. Earum doloremque magnam quidem saepe.', '248.00', NULL, 'DIGI103', 'instock', 0, 151, 'digital_2.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(2, 'et illo et qui', 'et-illo-et-qui', 'Recusandae eaque hic incidunt ut omnis ut numquam. Ipsa qui accusantium vitae optio veniam in. Sunt pariatur autem nesciunt alias et earum sequi.', 'Aut sapiente tempora ea eveniet itaque ut. Non velit cupiditate omnis qui et et autem. Vitae non et ut nulla similique. Veniam numquam iste error aperiam sint. Et officia eius sit. Non error consequatur aut consequatur totam dolores at sint. Deserunt eveniet eum et laudantium atque. Nihil officia omnis et beatae et. Eos ducimus molestiae eius aut ut.', '384.00', NULL, 'DIGI118', 'instock', 0, 172, 'digital_3.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(3, 'sint corrupti et eaque', 'sint-corrupti-et-eaque', 'Placeat accusamus et quia necessitatibus. Omnis occaecati voluptatem eum ipsum libero nisi. Qui delectus quisquam blanditiis laborum repellendus. Accusamus illo voluptatum numquam enim.', 'Voluptas itaque quis non sunt ducimus omnis explicabo animi. Ducimus voluptatem aliquid est sit consequatur. Et quia voluptatem et qui sit aut sunt. Eligendi neque repellendus quasi voluptas animi molestiae sed. Qui perspiciatis at voluptates sequi rerum. Suscipit sint excepturi praesentium nihil ut velit. Odio autem voluptates inventore nisi omnis in. Impedit odit voluptatem non qui reprehenderit recusandae qui. Animi dolorem natus occaecati quia. Quos nisi et harum repellendus minima dolores.', '147.00', NULL, 'DIGI393', 'instock', 0, 183, 'digital_13.jpg', NULL, 4, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(4, 'distinctio veritatis accusamus officia', 'distinctio-veritatis-accusamus-officia', 'Inventore et dolorem est sit eaque distinctio sint aliquam. Aut nihil alias ab aperiam. Impedit eligendi temporibus rerum et eaque reprehenderit ratione. Suscipit sed expedita qui nesciunt vitae.', 'Autem et eos itaque reiciendis rerum. Laudantium debitis earum tempora quo. Et quia qui eius officiis a. Reprehenderit voluptatem sed vitae velit voluptatem et. Aut ad ab vero modi molestiae officiis nihil. Temporibus facere impedit voluptatibus magnam amet. Magni et ut quos amet. Id ea fuga repellendus. Dolores quo exercitationem nobis. Rerum ut consectetur quasi dolorem ea. Sed laborum delectus nobis laudantium ut.', '37.00', NULL, 'DIGI294', 'instock', 0, 188, 'digital_7.jpg', NULL, 1, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(5, 'ipsam repudiandae qui in', 'ipsam-repudiandae-qui-in', 'Illo sed est autem deleniti dolor. Hic repellendus facere minus quos. Architecto et est quia dolorem.', 'Pariatur ipsa rem assumenda explicabo voluptatibus non. Facere voluptatibus laboriosam voluptates neque molestiae nihil sunt. Dolore hic ipsa sed nihil perspiciatis a ut quos. Dolorem perspiciatis sed nisi aliquid. Quia et dolorem nihil iusto sit omnis autem. Sunt dolores doloribus non vel dolorem. Porro rerum odio commodi aspernatur sed maiores distinctio. Commodi adipisci aut illo vel quis aut. Sapiente temporibus voluptas nemo mollitia quae illum quos.', '385.00', NULL, 'DIGI296', 'instock', 0, 152, 'digital_5.jpg', NULL, 3, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(6, 'ullam tempore dolore unde', 'ullam-tempore-dolore-unde', 'Aut mollitia esse officia dolores quis in. Reiciendis ut repellendus provident et sapiente cum id. Ut ea ipsum ut dolor omnis. Sint et est rerum ipsam molestiae.', 'Labore et qui eum enim. Esse et enim est nemo. Asperiores dignissimos similique itaque voluptate consectetur iusto culpa. Est ipsa voluptatem et quod saepe tempora non. Dolorem velit blanditiis praesentium ut sint dolores illo. Voluptatem quia laboriosam consequatur eum porro quia. Nam assumenda doloremque commodi possimus ut fuga. Alias vero quia quas. Delectus a ad id quis dolore quos sit iure.', '112.00', NULL, 'DIGI178', 'instock', 0, 163, 'digital_15.jpg', NULL, 4, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(7, 'modi enim voluptatem et', 'modi-enim-voluptatem-et', 'Voluptatem quaerat quisquam modi autem vero. Eveniet et mollitia in laudantium. Quia officia vel quaerat suscipit explicabo.', 'Autem quisquam vel dolor officia blanditiis id ipsum. Et sunt minima est vitae nihil. Vel iste deserunt qui est officiis cumque dolore. Reiciendis deleniti non ex occaecati. Qui velit aut placeat corporis qui. Quia nulla quae veritatis. Similique eius atque quis. Quasi quaerat natus veritatis ut consequuntur quae deserunt. Voluptas sed dignissimos voluptatum magni consequatur nihil. Reiciendis consequatur quisquam sed velit ab quibusdam est aut. Harum qui velit quas saepe autem nobis.', '323.00', NULL, 'DIGI473', 'instock', 0, 159, 'digital_4.jpg', NULL, 3, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(8, 'sed quaerat in qui', 'sed-quaerat-in-qui', 'Quo quia repudiandae voluptatem et doloribus doloremque et. Sed fugit maiores aperiam. Qui perspiciatis similique ad qui quia sit. Non eius nesciunt velit. Dolorem voluptas eum molestiae et sit sint.', 'Quibusdam et ratione sequi quaerat et eos enim repudiandae. Vel dolores maxime dolor sint. Velit magnam id molestiae explicabo dolore rem. Vero ea eos quas modi ut. Ex iusto voluptatibus cumque fuga autem ut. Qui tempore id pariatur enim et et. Itaque totam voluptatum fugit eveniet dicta natus labore. Vel et enim ducimus est porro. Mollitia consequatur pariatur commodi aut veritatis ut nobis.', '367.00', NULL, 'DIGI390', 'instock', 0, 125, 'digital_16.jpg', NULL, 4, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(9, 'voluptatibus reiciendis itaque distinctio', 'voluptatibus-reiciendis-itaque-distinctio', 'Voluptatem quidem provident ipsum assumenda. Labore ut delectus reprehenderit maiores. Et earum culpa veritatis dolor. Ab dolorem sed omnis consequatur nostrum aliquam. Facilis est quod molestias.', 'Itaque iure reiciendis et est cumque quis in. Quidem expedita quas occaecati ut sequi voluptatem. Repudiandae nobis quis consectetur dicta iste. Numquam tempora laborum distinctio est quasi. Repellendus quis pariatur hic omnis nemo maxime voluptatum. Eum repudiandae similique possimus veniam quia est aspernatur. Quia dicta perferendis et quam doloribus voluptatibus occaecati ex. Id dolor voluptatem labore veniam non. Qui perspiciatis dicta dolor assumenda pariatur et rerum vero.', '447.00', NULL, 'DIGI369', 'instock', 0, 160, 'digital_10.jpg', NULL, 5, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(10, 'culpa consequatur dolor voluptates', 'culpa-consequatur-dolor-voluptates', 'Nostrum voluptatem quasi unde molestias eos quo animi. Quia aliquam sed fuga quae sed enim maiores. Molestias cum laudantium est placeat numquam qui.', 'Est culpa in quisquam alias est sequi. Eum autem ipsa quaerat officiis quos. Commodi cupiditate a aspernatur delectus minima. Minus voluptate numquam eligendi consectetur tempore dolorum nostrum. Minima nihil deleniti eveniet et quis. Voluptas consequatur dolor doloribus aperiam nemo. Ullam ut aspernatur doloribus doloremque. Ea nihil occaecati quia earum expedita. Quod atque suscipit aliquid cumque expedita expedita id. Eum excepturi ut quia quod.', '374.00', NULL, 'DIGI297', 'instock', 0, 109, 'digital_12.jpg', NULL, 1, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(11, 'culpa dolorem quo adipisci', 'culpa-dolorem-quo-adipisci', 'Assumenda sapiente enim ad sequi. Laudantium et ut non labore ratione reprehenderit perspiciatis. In dolorem consequatur tempora culpa magnam voluptas quia. Eos reprehenderit magnam iste iste aut.', 'Tempora cum modi delectus enim reiciendis eveniet nulla unde. Exercitationem reiciendis voluptatem nisi accusamus deserunt sit voluptates. Qui voluptas tempora rerum id sunt. Voluptates praesentium quia sint suscipit dolorem repellat perspiciatis. Reprehenderit unde optio labore quo provident rem eveniet quae. Est rerum exercitationem quis adipisci doloremque quam. Et quibusdam est explicabo pariatur. Sapiente harum tenetur quia quod veniam ab. Ex adipisci velit ea ut consectetur.', '361.00', NULL, 'DIGI312', 'instock', 0, 136, 'digital_22.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(12, 'odit at aperiam facere', 'odit-at-aperiam-facere', 'Ut ut beatae aut consequuntur et est expedita. Ut enim dolorem explicabo non. Voluptatibus provident error rerum.', 'Incidunt placeat eos laudantium ea consequatur et. Magnam sint sit quis quis dolorum et totam. Placeat voluptate iusto reprehenderit. Iusto quod id nemo consequuntur sed quos officiis. Provident nihil esse et consequatur. Mollitia tempora autem sit ullam quos omnis qui. Dolor blanditiis nam animi. Dolorem a qui sequi nesciunt consequatur omnis consequuntur. Eaque rerum omnis vero minus. Enim rerum voluptatum laudantium et sit et. Pariatur ex aut nemo qui.', '251.00', NULL, 'DIGI239', 'instock', 0, 107, 'digital_19.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(13, 'temporibus eius eaque nostrum', 'temporibus-eius-eaque-nostrum', 'Amet sint nulla est debitis. Qui aut est esse. Ut nisi quo ea nulla eum iste molestiae accusamus. Cumque quam ut enim aperiam quo velit magni.', 'Est quod consequatur quos numquam beatae quidem sunt qui. Dicta veritatis cumque sed quae rerum. Provident dolorum quos ea nesciunt maiores ratione omnis eveniet. Ut perferendis sit ab repudiandae. Sunt quidem vel minima quis impedit. Quam sed quasi illum praesentium tempora voluptates. Voluptatem eos illo et.', '53.00', NULL, 'DIGI270', 'instock', 0, 193, 'digital_17.jpg', NULL, 3, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(14, 'earum voluptatum voluptatum facere', 'earum-voluptatum-voluptatum-facere', 'Autem odio rerum dolor ut iure. Vitae est at voluptatem aspernatur hic cum. Sint aut sunt sed reiciendis ipsum id.', 'Itaque ex provident nostrum saepe. Iure facilis aut sed ut amet minus praesentium. Animi omnis eos est accusantium minima dolores. Impedit quo at quis. Sed sint sit nam molestias nam sapiente. Voluptatem velit sapiente impedit. Eius molestiae ex itaque dignissimos. Cupiditate velit voluptatum doloribus voluptates accusantium dolor. Nisi dolor neque dolorem reiciendis ea amet. Molestiae fugit rerum sunt eos voluptatem sunt nisi. Quasi rem fugiat assumenda quo.', '307.00', NULL, 'DIGI101', 'instock', 0, 174, 'digital_14.jpg', NULL, 1, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(15, 'dolore voluptate quod iste', 'dolore-voluptate-quod-iste', 'Vel est est nisi aut nam. Id cum vel adipisci explicabo provident voluptas animi.', 'Qui error iste iure dolores praesentium quia. Vel laboriosam sunt nemo occaecati iste. Repudiandae ut sit exercitationem enim qui aperiam totam. Quidem adipisci id debitis occaecati provident ducimus. Unde fugit dolores et. Est quis voluptate cum et dolore et deleniti. Natus id optio dolores sed recusandae. Laboriosam aliquam et a. Id mollitia suscipit omnis numquam et consequuntur. Et eos occaecati reiciendis accusantium assumenda. Velit numquam ex debitis totam.', '348.00', NULL, 'DIGI337', 'instock', 0, 177, 'digital_20.jpg', NULL, 4, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(16, 'facilis amet error ex', 'facilis-amet-error-ex', 'Aut id ex quis sit rerum incidunt officiis. Vitae iure voluptatibus minus alias voluptatibus. Et sint animi omnis officiis optio laboriosam.', 'Natus reiciendis laboriosam natus itaque molestiae corporis. Est cum asperiores dignissimos nemo. Qui earum cum illum. Placeat ut harum et. Quam voluptatibus eum earum occaecati. Officia sit sit eveniet sint possimus. Ipsam repellat officia pariatur veritatis illum debitis. Nam quia recusandae est similique cum repellendus. At eum et ea non quaerat iste. Cumque incidunt quibusdam qui est iure.', '255.00', NULL, 'DIGI224', 'instock', 0, 178, 'digital_6.jpg', NULL, 3, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(17, 'asperiores dolorem fugiat iste', 'asperiores-dolorem-fugiat-iste', 'Corporis molestias harum nostrum non vel aut. Beatae qui laudantium aliquid. Velit iusto harum explicabo eligendi eos explicabo eius. Error et molestiae illo nam vero.', 'Rerum commodi doloribus facere corrupti ratione illo. Commodi consequatur eius voluptates facere rem et. Aliquam reiciendis non velit alias. Vel consequuntur officia voluptatibus reiciendis. Non asperiores beatae sed quam. Placeat iure est rerum vel tempora consectetur officia. Molestiae laboriosam dolor vitae consequuntur sapiente nam magni. Est nihil iusto odit tenetur sint consequatur nulla. Est aut pariatur sed ea animi ut quisquam.', '231.00', NULL, 'DIGI411', 'instock', 0, 118, 'digital_11.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(18, 'recusandae autem rerum facere', 'recusandae-autem-rerum-facere', 'Pariatur atque architecto explicabo explicabo incidunt eum in unde. Necessitatibus nihil eveniet sit. Animi suscipit saepe aut sapiente et. Excepturi qui delectus ad omnis quia omnis.', 'Ipsam facere qui magnam consequatur est adipisci. Ipsum ratione in animi id. Rem ratione repellat numquam. Odit sapiente voluptatem dolores. Sed accusantium in aliquid. Velit non dolor ipsam sint consequuntur dolorem explicabo rerum. Et ut debitis dolores rerum ut. Consequatur qui maiores qui harum. Harum et minima consectetur sed quidem dolor suscipit. Ea sunt non et adipisci. Velit labore qui quod architecto dolorum sint saepe.', '452.00', NULL, 'DIGI401', 'instock', 0, 138, 'digital_8.jpg', NULL, 5, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(19, 'consequuntur tenetur maxime repellendus', 'consequuntur-tenetur-maxime-repellendus', 'Eaque nisi repellat ab odio. Ut ut perferendis voluptatem aliquid autem qui. Beatae tempora repudiandae eum sit sunt voluptatem quia ea.', 'Dolore sequi quia excepturi itaque. Quod ducimus non reiciendis a deserunt. Ipsam et sunt placeat omnis. Dolorum accusamus consequuntur quia non voluptatem et dolore perspiciatis. Pariatur qui quibusdam quae necessitatibus ea voluptatem. Eaque excepturi sed aut omnis dolores. Veritatis et sint dolorem quis dicta ea.', '301.00', NULL, 'DIGI397', 'instock', 0, 194, 'digital_1.jpg', NULL, 1, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(20, 'voluptatibus qui provident quia', 'voluptatibus-qui-provident-quia', 'Explicabo incidunt ullam animi earum architecto. Adipisci quia ullam repellendus neque occaecati quod non. Perspiciatis esse molestiae ut est ullam ipsum. Aut molestiae earum sit.', 'Id labore debitis omnis. Autem saepe delectus ad sapiente ratione omnis. Corporis alias eaque voluptatum voluptatem suscipit illum rerum. Ut recusandae blanditiis repellat occaecati. Atque deleniti commodi sed placeat. Delectus accusamus adipisci vitae eaque quia ullam in facilis. Quia facere fuga optio eum. Dolorem nemo aut ipsa vel laboriosam aut non. Quo est et sint ut itaque unde reprehenderit. Temporibus dicta delectus illum consequatur. Autem dolor delectus quo porro.', '21.00', NULL, 'DIGI439', 'instock', 0, 122, 'digital_9.jpg', NULL, 5, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(21, 'corrupti est omnis enim', 'corrupti-est-omnis-enim', 'Explicabo ipsum voluptate quos aut. Aut labore in facilis distinctio. Cupiditate delectus reiciendis illum aut fugit qui autem.', 'Autem corrupti et quibusdam tenetur quia qui minus. Ipsam temporibus omnis ab voluptas laborum deleniti doloribus. Repudiandae culpa vero impedit quibusdam minima quidem. Dolorum nesciunt in dolores animi nemo non unde inventore. Sint vitae quia occaecati sunt quas ab sunt. Earum architecto aliquid possimus harum omnis. Esse minima modi ipsam reprehenderit. Tenetur quo voluptates qui ut harum.', '234.00', NULL, 'DIGI398', 'instock', 0, 131, 'digital_21.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21'),
(22, 'ab aspernatur doloribus iusto', 'ab-aspernatur-doloribus-iusto', 'Est et quis commodi nobis ipsam vitae ad. Corrupti sed cumque eum autem vitae sit. Aut illo neque vel voluptates molestiae.', 'Cumque modi est qui voluptate. Cum non corporis quis eos. Numquam et deserunt rerum qui. Consequatur culpa rerum alias aut molestiae. Dicta at amet vel eligendi quam debitis. Nobis quis laboriosam quis minus. Illo placeat sit eius possimus modi recusandae. Tempora maiores incidunt ea vero possimus dolores nemo. Animi veniam nihil laborum temporibus velit aut eaque quod. Praesentium a cupiditate porro consequatur qui voluptas. Omnis alias magnam reprehenderit blanditiis.', '422.00', NULL, 'DIGI152', 'instock', 0, 126, 'digital_18.jpg', NULL, 2, '2022-03-13 02:17:21', '2022-03-13 02:17:21');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('dBhl0lCh9nG1sqMbqagBpg6m8omTePOh9Svpv9UI', 1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 'YTo3OntzOjY6Il90b2tlbiI7czo0MDoiQjNWRXczbzRVSE4xSHhjMVNSNzk0NjJ4ZWtiOGNzZ293SVE3cmZNbSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwNCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NDoiY2FydCI7YToxOntzOjc6ImRlZmF1bHQiO086Mjk6IklsbHVtaW5hdGVcU3VwcG9ydFxDb2xsZWN0aW9uIjoyOntzOjg6IgAqAGl0ZW1zIjthOjA6e31zOjI4OiIAKgBlc2NhcGVXaGVuQ2FzdGluZ1RvU3RyaW5nIjtiOjA7fX1zOjUwOiJsb2dpbl93ZWJfNTliYTM2YWRkYzJiMmY5NDAxNTgwZjAxNGM3ZjU4ZWE0ZTMwOTg5ZCI7aToxO3M6NDoiYXV0aCI7YToxOntzOjIxOiJwYXNzd29yZF9jb25maXJtZWRfYXQiO2k6MTY0NzIzOTg0Njt9czoxNzoicGFzc3dvcmRfaGFzaF93ZWIiO3M6NjA6IiQyeSQxMCRSQnpNWEdSaUlGZE5SeEdhRFFHeE5lL213ZXUza1VmbGc1OXBrdlZzTHNxUzFsaW9HSXFZUyI7fQ==', 1647239846),
('jSJRoZ0XArucq0kElqLfogINkeDZUThtx6ouJTlC', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 'YTo0OntzOjY6Il90b2tlbiI7czo0MDoiZ3k4aU9aM240eVlvRnMxeEcxSDVBN0JhckxxOWVPWU50OHRIU20xMCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MzU6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwNC9hdXRoL2ZhY2Vib29rIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo1OiJzdGF0ZSI7czo0MDoiR29JUzdQb0tSTVFFZ1ZiTWJyemU3SU5HMEg5RnlIc09vZGVmZmNpTCI7fQ==', 1647187576),
('LxPestmlOQh0MKzQHBSoPHgJRCIZfdFoBULSb2QK', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.51 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiNll0TURGY1BLZGJvZzhUQzk0Y3NST1BEejlubWNKOG5VZW5OeGswSSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MjE6Imh0dHA6Ly8xMjcuMC4wLjE6ODAwNCI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fXM6NDoiY2FydCI7YTowOnt9czo1OiJzdGF0ZSI7czo0MDoiZ0xqUVBZeDZCWDd1ZnpySGVLMVFSdGJGSHk1NkFmOTMwaHJsUUlwdiI7fQ==', 1647187304);

-- --------------------------------------------------------

--
-- Table structure for table `type_product`
--

CREATE TABLE `type_product` (
  `typeid` int(11) NOT NULL,
  `typename` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_product`
--

INSERT INTO `type_product` (`typeid`, `typename`) VALUES
(200, 'Điện Thoại'),
(201, 'Ti vi'),
(202, 'Máy lạnh'),
(203, 'Máy giặt'),
(204, 'Tủ lạnh'),
(205, 'Nồi cơm điện'),
(206, 'Bình Đun'),
(207, 'Tai Nghe');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `create_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user' COMMENT 'role for user',
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `create_date`, `provider_id`, `facebook_id`, `google_id`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `role`, `avatar`, `created_at`, `updated_at`) VALUES
(1, 'akiet', NULL, NULL, NULL, NULL, 'admin@gmail.com', NULL, '$2y$10$RBzMXGRiIFdNRxGaDQGxNe/mweu3kUflg59pkvVsLsqS1lioGIqYS', NULL, NULL, NULL, NULL, NULL, 'admin', NULL, '2022-02-25 09:58:10', '2022-02-25 09:58:10'),
(2, 'akiet', NULL, NULL, NULL, NULL, 'admin1@gmail.com', NULL, '$2y$10$BxIsT7CNWP1ND5tuvrvARuvYuiuSohwH6qIc.n2VhSOrrFL.MlpSu', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2022-02-25 10:05:52', '2022-02-25 10:05:52'),
(3, 'akiet2', NULL, NULL, NULL, NULL, 'user@gmail.com', NULL, '$2y$10$23ohfhj6IJvoUmp/KOTfve..PMqBAtkR9VDhGJnBjVIuN43Y3XM9m', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2022-02-25 10:07:12', '2022-02-25 10:07:12'),
(4, 'SHOP', NULL, NULL, NULL, NULL, 'shop@gmail.com', NULL, '$2y$10$KtVYhFeQAL3gPlfeWrAT/.cWc8QJm0SydxUcxnNZfz3sDJTIluEJq', NULL, NULL, NULL, NULL, NULL, 'shop', NULL, '2022-03-08 00:01:40', '2022-03-08 00:01:40'),
(5, 'Shopee', NULL, NULL, NULL, NULL, 'shopee@gmail.com', NULL, '$2y$10$/WO1309Vm36U4po4AOFpaOHv17ei2V5MxYFD1TI/vsXMwqFbzNtX2', NULL, NULL, NULL, NULL, NULL, 'shop', NULL, '2022-03-09 00:38:10', '2022-03-09 00:38:10'),
(6, 'Lazada', NULL, NULL, NULL, NULL, 'lazada@gmail.com', NULL, '$2y$10$LemTzkgH31qSpOVP9TcAzenHXXSNqvTWgPK3lANu2yfPJ3nhBgRgu', NULL, NULL, NULL, NULL, NULL, 'shop', NULL, '2022-03-09 00:38:54', '2022-03-09 00:38:54'),
(7, 'Shop Asus', NULL, NULL, NULL, NULL, 'asus@gmail.com', NULL, '$2y$10$xMgmsH0.osK2LAVLqicHa.kq2VCudeyhKTlQvfhksDsgQmeLnfwf.', NULL, NULL, NULL, NULL, NULL, 'shop', NULL, '2022-03-09 00:43:39', '2022-03-09 00:43:39'),
(9, 'Chau Giang', NULL, NULL, NULL, NULL, 'deocoemail@gmail.com', NULL, '$2y$10$18QLz5UhAxIWXdHZgAJ0ReOiP85V/muQVjdYOrltKFzB.BIA0/9fi', NULL, NULL, NULL, NULL, NULL, 'user', NULL, '2022-03-11 03:31:01', '2022-03-11 03:31:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_name_unique` (`name`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`categoryid`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`manufacturerid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productid`),
  ADD KEY `fk_type` (`type`),
  ADD KEY `fk_category` (`categoryid`),
  ADD KEY `fk_manufracturer` (`manufacturerid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- Indexes for table `type_product`
--
ALTER TABLE `type_product`
  ADD PRIMARY KEY (`typeid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `categoryid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manufacturer`
--
ALTER TABLE `manufacturer`
  MODIFY `manufacturerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1007;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `type_product`
--
ALTER TABLE `type_product`
  MODIFY `typeid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`categoryid`) REFERENCES `category` (`categoryid`),
  ADD CONSTRAINT `fk_manufacturer` FOREIGN KEY (`manufacturerid`) REFERENCES `manufacturer` (`manufacturerid`),
  ADD CONSTRAINT `fk_type_product` FOREIGN KEY (`type`) REFERENCES `type_product` (`typeid`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
